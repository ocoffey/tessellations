# Tessellations

## Idea

Use recursion to generate a list of tessellating patterns for varying unit block sizes

Initially planning for an 8 unit block shape

>Manually Created Examples

🟦🟩🟨🟧⬛

⬛⬛⬛⬛🟨⬛🟨⬛  
⬛🟩⬛🟩🟨🟨🟨⬛  
⬛🟩🟩🟩🟨🟨⬛⬛  
⬛🟩🟩🟧🟨🟧⬛⬛  
🟦🟩🟦🟧🟧🟧⬛⬛  
🟦🟦🟦🟧🟧⬛⬛⬛  
🟦🟦⬛🟧⬛⬛⬛⬛  
🟦⬛⬛⬛⬛⬛⬛⬛

⬛⬛⬛⬛⬛⬛🟨🟨  
⬛⬛⬛🟩🟩🟨🟨⬛  
⬛⬛🟩🟩🟨🟨🟨⬛  
⬛🟩🟩🟩🟨🟧🟧⬛  
⬛🟩🟦🟦🟧🟧⬛⬛  
⬛🟦🟦🟧🟧🟧⬛⬛  
🟦🟦🟦🟧⬛⬛⬛⬛  
🟦⬛⬛⬛⬛⬛⬛⬛

⬛⬛🟦⬛⬛⬛⬛⬛  
⬛🟦🟦🟦⬛⬛⬛⬛  
⬛🟦🟩🟦🟩⬛⬛⬛  
⬛🟦🟩🟦🟩⬛⬛⬛  
⬛🟧🟩🟩🟩⬛⬛⬛  
🟧🟧🟧🟩⬛⬛⬛⬛  
🟧🟨🟧🟨⬛⬛⬛⬛  
🟧🟨🟧🟨⬛⬛⬛⬛  
⬛🟨🟨🟨⬛⬛⬛⬛  
⬛⬛🟨⬛⬛⬛⬛⬛

## Limitations:

* Must be within a 16x16 grid
  * No more than 16 unit blocks used for the tessellated shape
  * Initial runs are on an 8 unit block "shape"
* No ridiculous tessellations
  * Nothing where there's just one stray pixel that "tessellates" but it's multiple screens of tessellation away
  * Can only tessellate into the surrounding 16x16 grids from the source grid

## Purpose

I was manually coming up with 8 unit block tessellations, then realized, "surely, there must be an easier way to generate all of these and just *view* them, then pick which I'd like to use"

**THERE IS, IT'S CALLED USING MATH**

Will probably prototype in Python, then swap to Rust for larger inputs/handling recursion more speedily
