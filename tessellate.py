
"""
Steps:
1. Create "shapes", which are mathematical representations of 8 unit block figures
    * Maybe use a bitmap to store?
    * Method should be iterative?
2. Create a form of test for repeatability/tessellation
    * How to test for "shape" repeatability...
    * It's really a test in how to define physical patterns
3. Print the shapes as their tessellations
    * Use nice color block emoji for maximum effect

Uh oh
Maybe this uses some weird matrix operations?

If it does it's fine I guess a little actual math sometimes never hurt anybody

Maybe we start with 4 unit block shapes and work our way up from there, yeah?
    * all fit within a 4x4 grid, all are standard Tetris pieces

Make it so that if a piece doesn't fit/goes offgrid, try and place that piece at the loop in the grid?
    * set the grid/bitmap to 'null' values specifically, to tell when a piece should try and loop
"""

# hard coding some shapes, initial iteration
# realizing the redundancy means these would work better as objects; don't care for right now, but will amend later

iblock = [[0 for i in range(4)] for j in range(4)]
for row in iblock:
    row[0] = 1
    print(row)
print()

tblock = [[0 for u in range(4)] for j in range(4)]
tblock[3][2] = tblock[3][1] = tblock[3][0] = tblock[2][1] = 1
for row in tblock:
    print(row)
print()

oblock = [[0 for u in range(4)] for j in range(4)]
oblock[1][1] = oblock[2][1] = oblock[1][2] = oblock[2][2] = 1
for row in oblock:
    print(row)
print()

sblock = [[0 for u in range(4)] for j in range(4)]
sblock[3][0] = sblock[3][1] = sblock[2][1] = sblock[2][2] = 1
for row in sblock:
    print(row)
print()

zblock = [[0 for u in range(4)] for j in range(4)]
zblock[2][0] = zblock[2][1] = zblock[3][1] = zblock[3][2] = 1
for row in zblock:
    print(row)
print()

lblock = [[0 for u in range(4)] for j in range(4)]
lblock[3][0] = lblock[3][1] = lblock[3][2] = lblock[2][2] = 1
for row in lblock:
    print(row)
print()

jblock = [[0 for u in range(4)] for j in range(4)]
jblock[2][0] = jblock[3][0] = jblock[3][1] = jblock[3][2] = 1
for row in jblock:
    print(row)
print()